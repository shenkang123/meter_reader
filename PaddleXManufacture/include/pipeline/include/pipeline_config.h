// Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#endif
#include <iostream>
#include <string>
#include <vector>
#include "yaml-cpp/yaml.h"

#include "pipeline/include/pipeline_utils.h"

namespace PaddleXPipeline {

bool CheckPipelineName(const std::string &cfg_file,
                       const YAML::Node &item);


bool CheckPipelineNodes(const std::string &cfg_file,
                        const YAML::Node &item);


bool CheckNode(const std::string &cfg_file,
               const std::string &node_name,
               const YAML::Node &item);

bool CheckSource(const std::string &node_name,
                 const std::string &cfg_file,
                 const YAML::Node &item);

bool CheckDecode(const std::string &node_name,
                 const std::string &cfg_file,
                 const YAML::Node& item);

bool CheckPredict(const std::string &node_name,
                  const std::string &cfg_file,
                  const YAML::Node& item);

bool CheckVisualize(const std::string &node_name,
                    const std::string &cfg_file,
                    const YAML::Node& item);

bool CheckSink(const std::string &node_name,
               const std::string &cfg_file,
               const YAML::Node& item);

bool CheckRoiCrop(const std::string &node_name,
                  const std::string &cfg_file,
                  const YAML::Node& item);

bool CheckResize(const std::string &node_name,
                 const std::string &cfg_file,
                 const YAML::Node& item);

bool CheckFilterBbox(const std::string &node_name,
                     const std::string &cfg_file,
                     const YAML::Node& item);

bool CheckVisualize(const std::string &node_name,
                    const std::string &cfg_file,
                    const YAML::Node& item);

bool CheckKey(const std::string &node_name,
              const std::string &cfg_file,
              const YAML::Node& item,
              const std::string &key);

bool CheckPath(const std::string &node_name,
               const std::string &cfg_file,
               const std::string &path);


bool CheckUndefinedNext(const std::vector<std::string> &node_names,
                        const YAML::Node &nodes,
                        const std::string &cfg_file);

bool CheckNodeWithoutInput(const YAML::Node &nodes,
                           const std::string &cfg_file);

bool CheckNodeIsDefined(const YAML::Node &nodes,
                        const std::string &cfg_file,
                        const std::string &type);

}  // namespace PaddleXPipeline
