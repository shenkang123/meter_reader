// Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#ifdef _WIN32
#include <direct.h>
#include <io.h>
#else  // Linux/Unix
#include <dirent.h>
// #include <sys/io.h>
#if defined(__arm__) || defined(__aarch64__)  // for arm
#include <aarch64-linux-gnu/sys/stat.h>
#include <aarch64-linux-gnu/sys/types.h>
#else
#include <sys/stat.h>
#include <sys/types.h>
#endif
#include <unistd.h>
#endif

#ifdef _WIN32
#define OS_PATH_SEP "\\"
#else
#define OS_PATH_SEP "/"
#endif

#if defined(_WIN32)
#ifdef PADDLEX_DEPLOY
#define PD_INFER_DECL __declspec(dllexport)
#else
#define PD_INFER_DECL __declspec(dllimport)
#endif  // PADDLEX_DEPLOY
#else
#define PD_INFER_DECL
#endif  // _WIN32

namespace PaddleXPipeline {

bool IsMember(const std::string &object,
              const std::vector<std::string> &group);

PD_INFER_DECL bool GenerateSavePath(const std::string& save_dir,
                      const std::string& file_path,
                      std::string *save_path);

}  // PaddleXPipeline
