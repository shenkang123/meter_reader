// Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <map>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "yaml-cpp/yaml.h"
#include "model_deploy/common/include/paddle_deploy.h"
#include "pipeline/include/pipeline_nodes.h"
#include "pipeline/include/pipeline_config.h"
#include "pipeline/include/pipeline_utils.h"

#if defined(_WIN32)
#ifdef PADDLEX_DEPLOY
#define PD_INFER_DECL __declspec(dllexport)
#else
#define PD_INFER_DECL __declspec(dllimport)
#endif  // PADDLEX_DEPLOY
#else
#define PD_INFER_DECL
#endif  // _WIN32

namespace PaddleXPipeline {

class PD_INFER_DECL Pipeline {
 public:
  Pipeline() {}
  explicit Pipeline(
    const std::string pipeline_name) : pipeline_name_(pipeline_name) {}

  bool Init(const std::string &cfg_file)
  {
    if (!ParseConfig(cfg_file)) return false;
    if (!BuildPipeline()) return false;
    return true;
  }

  bool SetInput(const std::string &node_name,
                const std::vector<std::string> &image_path);
  bool SetInput(const std::string &node_name,
                const std::vector<cv::Mat> &image);
  bool Run();
  bool GetOutput(const std::string &node_name,
                 std::vector<PaddleDeploy::Result> *result);
  std::string Name() {return pipeline_name_; }

 protected:
  YAML::Node yaml_config_;
  bool ParseConfig(const std::string &cfg_file);
  bool BuildPipeline();

  std::map<std::string, std::shared_ptr<PipelineNode>> nodes_;
  std::vector<std::shared_ptr<PipelineNode>> sorted_nodes_;


 private:
  bool LinkNodes();
  bool CreateNodes(const YAML::Node &nodes_info);
  bool GetSourceNodes();
  bool GetSinkNodes();
  bool TopoSort();
  std::shared_ptr<PipelineNode> CreateNode(
    const std::string &name, const YAML::Node &node);
  std::vector<std::shared_ptr<PipelineNode>> GetNode(
    const std::vector<std::string> &names);
  std::vector<std::string> source_nodes_;
  std::vector<std::string> sink_nodes_;
  std::string pipeline_name_;
};

}  // namespace PaddleXPipeline
