// Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <time.h>
#include <sys/timeb.h>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <memory>

#include "yaml-cpp/yaml.h"
#include "model_deploy/common/include/paddle_deploy.h"
#include "model_deploy/utils/include/bbox_utils.h"
#include "model_deploy/utils/include/visualize.h"
#include "pipeline/include/pipeline_data.h"
#include "pipeline/include/pipeline_utils.h"


namespace PaddleXPipeline {

class PipelineNode {
 public:
  PipelineNode(const std::string &node_name, const YAML::Node &item)  {
    name_ = node_name;
    SetOutputNodes(item);
    count_ = 0;
  }
  virtual bool Run(
    const std::vector<std::shared_ptr<PipelineNode>> &input_nodes) = 0;
  virtual bool GetOutput(DataBlob *data) {
    *data = data_;
    return true;
  }
  virtual bool ReleaseOutput() {
    data_.Clear();
    return true;
  }
  virtual bool AddInputNodes(const std::string &node_name) {
    input_nodes_.push_back(node_name);
    return true;
  }
  virtual bool CheckInputNodes() {
    if (input_nodes_.size() != 1) {
      std::cerr << "[Error] node(" << name_
                << ") should have one input node, but recieved "
                << input_nodes_.size() << " nodes" << std::endl;
      return false;
    } else {
      return true;
    }
  }
  virtual std::string Name() { return name_; }
  virtual std::string Type() { return "PipelineNode"; }
  virtual bool GetInputNodes(std::vector<std::string> *input_nodes) {
    *input_nodes = input_nodes_;
    return true;
  }
  virtual bool GetOutputNodes(std::vector<std::string> *output_nodes) {
    *output_nodes = output_nodes_;
    return true;
  }
  virtual bool SetDataPath(const std::vector<std::string> &img_path) {
    data_.img_paths.clear();
    data_.img_paths = img_path;
    return true;
  }
  virtual bool SetDataImage(const std::vector<cv::Mat> &image) {
    data_.imgs.clear();
    data_.imgs = image;
    return true;
  }
  int count_;

 protected:
  bool SetOutputNodes(const YAML::Node &item);
  std::string name_;
  std::vector<std::string> input_nodes_;
  std::vector<std::string> output_nodes_;
  DataBlob data_;
};

class Source : public PipelineNode {
 public:
  Source(const std::string &node_name, const YAML::Node &item) :
    PipelineNode(node_name, item) {}
  bool Run(const std::vector<std::shared_ptr<PipelineNode>> &input_nodes);
  bool CheckInputNodes() { return true; }
  std::string Type() { return "Source"; }
};


class Decode : public PipelineNode {
 public:
  Decode(const std::string &node_name, const YAML::Node &item) :
    PipelineNode(node_name, item) {}
  bool Run(const std::vector<std::shared_ptr<PipelineNode>> &input_nodes);
  std::string Type() { return "Decode"; }
};


class Resize : public PipelineNode {
 public:
  Resize(const std::string &node_name, const YAML::Node &item) :
    PipelineNode(node_name, item) {
    resizer->Init(item["init_params"]);
    height = item["init_params"]["height"].as<int>();
    width = item["init_params"]["width"].as<int>();
    if (item["init_params"]["interp"].IsDefined()) {
      interp = item["init_params"]["interp"].as<int>();
    } else {
      interp = 1;
    }
  }
  bool Run(const std::vector<std::shared_ptr<PipelineNode>> &input_nodes);
  std::string Type() { return "Resize"; }

 private:
  std::shared_ptr<PaddleDeploy::Transform> resizer =
    std::make_shared<PaddleDeploy::Resize>();
  int width;
  int height;
  int interp;
};

class Predict : public PipelineNode {
 public:
  Predict(const std::string &node_name, const YAML::Node &item) :
    PipelineNode(node_name, item) {
    YAML::Node init_params = item["init_params"];
    model_dir_ = init_params["model_dir"].as<std::string>();
    use_gpu_ = false;
    gpu_id_ = 0;
    use_trt_ = false;
    if (init_params["use_gpu"].IsDefined())
      use_gpu_ = init_params["use_gpu"].as<bool>();
    if (init_params["gpu_id"].IsDefined())
      gpu_id_ = init_params["gpu_id"].as<int>();
    if (init_params["use_trt"].IsDefined())
      use_trt_ = init_params["use_trt"].as<bool>();

    std::string model_file = model_dir_ + "/model.pdmodel";
    std::string params_file = model_dir_ + "/model.pdiparams";
    std::string config_file = model_dir_ + "/model.yml";


    model_->Init(config_file);

    // inference engine init
    PaddleDeploy::PaddleEngineConfig engine_config;
    engine_config.model_filename = model_file;
    engine_config.params_filename = params_file;
    engine_config.use_gpu = use_gpu_;
    engine_config.gpu_id = gpu_id_;
    if (model_->preprocess_->GetModelName() == "DeepLabV3P" ||
        model_->preprocess_->GetModelName() == "UNet" ||
        model_->preprocess_->GetModelName() == "FastSCNN" ||
        model_->preprocess_->GetModelName() == "HRNet" ||
        model_->preprocess_->GetModelName() == "BiSeNetV2") {
      use_trt_ = false;
    }
    if (use_trt_) {
      engine_config.use_trt = use_trt_;
      engine_config.precision = 0;
      engine_config.min_subgraph_size = 10;
      engine_config.max_workspace_size = 1 << 30;
    }
    model_->PaddleEngineInit(engine_config);
  }
  bool Run(const std::vector<std::shared_ptr<PipelineNode>> &input_nodes);
  std::string Type() { return "Predict"; }

  ~Predict() {
    delete model_;
    model_ = NULL;
  }

 private:
  PaddleDeploy::Model* model_ =
    PaddleDeploy::CreateModel("paddlex");
  std::string model_dir_;
  bool use_gpu_;
  int gpu_id_;
  bool use_trt_;
};


class RoiCrop : public PipelineNode {
 public:
  RoiCrop(const std::string &node_name, const YAML::Node &item) :
    PipelineNode(node_name, item) {}
  bool Run(const std::vector<std::shared_ptr<PipelineNode>> &input_nodes);
  bool CheckInputNodes() {
    if (input_nodes_.size() != 2) {
      std::cerr << "[Error] node(" << name_
                << ") should have two input nodes, but recieved "
                << input_nodes_.size() << " nodes" << std::endl;
      return false;
    } else {
      return true;
    }
  }
  std::string Type() { return "RoiCrop"; }
};

class FilterBbox : public PipelineNode {
 public:
  FilterBbox(const std::string &node_name, const YAML::Node &item) :
    PipelineNode(node_name, item) {
    if (item["init_params"]["score_thresh"].IsDefined()) {
      score_thresh = item["init_params"]["score_thresh"].as<float>();
    }
  }
  bool Run(const std::vector<std::shared_ptr<PipelineNode>> &input_nodes);
  std::string Type() { return "FilterBbox"; }

 private:
  float score_thresh;
};

class Visualize : public PipelineNode {
 public:
  Visualize(const std::string &node_name, const YAML::Node &item) :
    PipelineNode(node_name, item) {
    save_dir = item["init_params"]["save_dir"].as<std::string>();
  }
  bool Run(const std::vector<std::shared_ptr<PipelineNode>> &input_nodes);
  bool CheckInputNodes() {
    if (input_nodes_.size() != 2) {
      std::cerr << "[Error] node(" << name_
                << ") should have two input nodes, but recieved "
                << input_nodes_.size() << " nodes" << std::endl;
      return false;
    } else {
      return true;
    }
  }
  std::string Type() { return "Visualize"; }

 private:
  std::string save_dir;
};

class Sink : public PipelineNode{
 public:
  Sink(const std::string &node_name, const YAML::Node &item) :
    PipelineNode(node_name, item) {}
  bool Run(const std::vector<std::shared_ptr<PipelineNode>> &input_nodes);
  std::string Type() { return "Sink"; }
};

}  // namespace PaddleXPipeline
