// Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "model_deploy/common/include/paddle_deploy.h"

namespace PaddleXPipeline {

struct DataBlob {
  std::vector<cv::Mat> imgs;
  std::vector<std::string> img_paths;
  std::vector<PaddleDeploy::Result> results;

  DataBlob() = default;

  void Clear() {
    imgs.clear();
    img_paths.clear();
    results.clear();
  }

  DataBlob(const DataBlob& data) {
    Clear();
    imgs = data.imgs;
    img_paths = data.img_paths;
    results = data.results;
  }

  DataBlob& operator=(const DataBlob& data) {
    Clear();
    imgs = data.imgs;
    img_paths = data.img_paths;
    results = data.results;
    return *this;
  }

  ~DataBlob() {
    Clear();
  }
};

}  // namespace PaddleXPipeline
