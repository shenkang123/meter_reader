# 指针仪表度数检测

#### 介绍
* paddlex_指针仪表读数_cpu_windows

#### 环境
* win10 + vs2017/2019 + only cpu

#### 使用说明
* 打开工程，修改opencv、PaddleXManufacture包含目录和库目录路径
* 下载百度官方提供的检测和分割模型，解压后放在meter_reader目录下

  | 表计检测预训练模型 | 刻度和指针分割预训练模型 |
  | -- | -- |
  | [meter_det_model](https://bj.bcebos.com/paddlex/examples2/meter_reader/meter_det_model.tar.gz) | [meter_seg_model](https://bj.bcebos.com/paddlex/examples2/meter_reader//meter_seg_model.tar.gz) |

* 修改meter_reader.cpp中图片路径
* 必须在release下编译



#### 参考
* https://gitee.com/paddlepaddle/PaddleX/tree/develop/examples/meter_reader

