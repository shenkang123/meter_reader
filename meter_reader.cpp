#include <string>
#include <iostream>
#include <vector>
#include <utility>
#include <limits>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/core.hpp>
#include "pipeline/include/pipeline.h"
#include "reader_postprocess.h"

using namespace std;
using namespace cv;


void get_image_names(std::string file_path, std::vector<std::string>& file_names)
{
	intptr_t hFile = 0;
	_finddata_t fileInfo;
	hFile = _findfirst(file_path.c_str(), &fileInfo);
	if (hFile != -1)
	{
		do 
		{
			//如果为文件夹，可以通过递归继续遍历，此处我们不需要
			if ((fileInfo.attrib & _A_SUBDIR))
			{
				continue;
			}
			//如果为单个文件直接push_back
			else
			{
				file_names.push_back(fileInfo.name);
			}

		}
		while (_findnext(hFile, &fileInfo) == 0);

		_findclose(hFile);
	}
}



int main()
{
    std::vector<cv::Mat> images; 
	PaddleXPipeline::Pipeline pipeline;

	std::vector<std::string> file_names;
	string dir = "E:/meter/meter_det/images/train/";
	get_image_names(dir+"*.jpg", file_names);

	//https://gitee.com/paddlepaddle/PaddleX/tree/develop/examples/meter_reader
	for (auto file : file_names)
	{
		cv::Mat src = cv::imread(dir + file);
		images.push_back(src);
		if (pipeline.Init("meter_pipeline.yml"))
		{
			pipeline.SetInput("src0", images);
			pipeline.Run();//开始分析，自动生成检测和分割结果文件夹
			std::vector<PaddleDeploy::Result> det_results;
			std::vector<PaddleDeploy::Result> seg_results;
			pipeline.GetOutput("sink0", &det_results);
			pipeline.GetOutput("sink1", &seg_results);

			//图像腐蚀->环形展开->转一维图像->二值化-定位指针相对刻度位置->根据刻度根数判断表盘类型->指针相对位置*量程得到度数
			std::vector<std::vector<uint8_t>> seg_label_maps;
			Erode(5, seg_results, &seg_label_maps);

			//获取角度结果
			std::vector<float> results;
			GetMeterReading(seg_label_maps, &results);
			PrintMeterReading(results);
			cv::Mat vis_img;
			Visualize(src, det_results[0], results, &vis_img);
			cv::imwrite("result/" + file, vis_img);
		}
	}
	return 0;
}
